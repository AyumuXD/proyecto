<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitas', function (Blueprint $table) {
            $table->bigIncrements('id_visitas');
            $table->dateTime('fecha');
            $table->string('nombres',50);
            $table->string('tipo_doc',10);
            $table->string('numero_doc',11);
            $table->string('motivo',30);
            $table->string('empleado',50);
            $table->string('cargo',20);
            $table->string('oficina',50);
            $table->dateTime('fecha_salida')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitas');
    }
};
