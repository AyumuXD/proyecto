<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visitas extends Model
{
    protected $fillable = [
        'fecha',
        'nombres',
        'tipo_doc',
        'numero_doc',
        'motivo',
        'empleado',
        'cargo',
        'oficina',
        'fecha_salida',

    ];
    public $timestamps = false;
}

