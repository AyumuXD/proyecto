<?php

namespace App\Http\Controllers;

use App\Http\Requests\VisitasRequest;
use App\Models\Visitas;
use Illuminate\Http\Request;
use DB;
class VisitasController extends Controller
{
    public function index(){
        $visitas = Visitas::all();
         return view('index')->with([
             'listados' => $visitas,
         ]);
    }
    public function store(VisitasRequest $request){
        $request = Visitas::create($request->validated());
        return redirect('/');
    }
}