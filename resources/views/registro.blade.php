<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Registtro</title>
</head>
<body>
    <main class="container-fluid">
        
        <form action="{{route('registro')}}" method="post">
            @csrf
            <label for="fecha">Fecha ingreso</label>
            <input class="form-control" type="date" name="fecha" id="fecha">
        
        
            <label for="nombres">Nombres</label>
            <input class="form-control" type="text" name="nombres" id="nombres">
            
            <select class="form-control" name="tipo_doc" id="tipo_doc">
                <option selected disabled>Seleccione el tipos de archivo</option>
                <option value="DNI">DNI</option>
                <option value="RUC">RUC</option>
            </select>
        
            <label for="numero_doc">N° documento</label>
            <input class="form-control" type="text" name="numero_doc" id="numero_doc">
            
            <select class="my-3 form-control" name="cargo" id="cargo">
                <option selected disabled>Seleccione el tipos de cargo</option>
                <option value="Gerente">Gerente</option>
                <option value="Administrador">Administrador</option>
                <option value="Coodinador">Coodinador</option>
            </select>
        
            <select class="my-3 form-control" name="oficina" id="oficina">
                <option selected disabled>Seleccione el tipos de oficina</option>
                <option value="Gerente">Administracion</option>
                <option value="Administrador">Gerente</option>
                <option value="Coodinador">Coodinador</option>
            </select>
        
            <label for="empleado">Empleado</label>
            <input class="form-control" type="text" name="empleado" id="empleado">
        
            <label for="motivo">Motivo</label>
            <input class="form-control" type="text" name="motivo" id="motivo">
            <button type="submit">XD</button>
            <input type="submit" class="btn btn-success my-4" value="Enviar">
            
            
            
        </form>
    </main>
    
</body>
</html>