<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Proyecto</title>
</head>
<body>
    <nav class="container-fluid my-2">
        <a href="Nuevo" class="btn btn-secondary">NUEVO REGISTRO</a>
    </nav>
    
    <main class="container-fluid">
        <table class="table">
            <thead>
                <th>Fecha de ingreso</th>
                <th>Visitante</th>
                <th>N° Documento</th>
                <th>Oficina</th>
                <th>Empleo</th>
                <th>Motivo</th>
                <th>Fecha de salida</th>
                <th>Acción</th>
            </thead>
            <tbody>
                @foreach ($listados as $listado)
                    <tr>
                        <td>{{$listado->fecha}}</td>
                        <td>{{$listado->nombres}}</td>
                        <td>{{$listado->numero_doc}}</td>
                        <td>{{$listado->oficina}}</td>
                        <td>{{$listado->empleado}}</td>
                        <td>{{$listado->motivo}}</td>
                        <td>{{$listado->fecha_salida}}</td>
                        <td> <a type="button" class="btn btn-warning">Ediar</a> <a type="button" class="btn btn-success">Detalle </a></td>
                    </tr>
                    
                @endforeach
                
            </tbody>
        </table>
    </main>
</body>
</html>